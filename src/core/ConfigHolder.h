/**
 * FileName: ConfigHolder.h
 * Used to define the class ConfigHolder which is used to hold the config files.
 *
 * Copyright (C) 2008 Kermit Mei <kermit.mei@gmail.com>
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation.
 *
 * Written by Kermit Mei <kermit.mei@gmail.com>
 *
 * Many of the ideas implemented here are from the author's experiment.
 * But the dictionary's format coincide with the other word recite software
 * to help the users get more available data. And the review times designed
 * by means of the theory named Forgetting Curve which dicoveried by the
 * German psychologist named Hermann Ebbinghaus(1850–1909).
 *
 **/ 

#ifndef CONFIGHOLDER_H
#define CONFIGHOLDER_H

#include <string>
#include <vector>

namespace freeRecite {

class ConfigHolder;
extern ConfigHolder configHolder;

class ConfigHolder
{
public:
  ConfigHolder();
  void setRootDir(const char *dir);

  const std::string rootDir() const;
  const std::string tasksDir() const;
  const std::string mgrFile() const;
  const std::string localDictFile() const;
  const std::string globalDictFile() const ;
  const std::string keyFile() const;
  const std::string doneFile() const;
  const std::vector<unsigned> *r_list();
  const std::vector<unsigned> *t_list();
  const std::vector<unsigned> *e_list();
private:
  std::string rootDirectory;
  std::vector<unsigned> initPt;
};

inline
const std::string ConfigHolder::doneFile() const {
  return rootDirectory + "tasks/done.txt";
}

inline
const std::string ConfigHolder::rootDir() const {
  return rootDirectory;
}

inline
const std::string ConfigHolder::tasksDir() const {
  return rootDirectory + "tasks/";
}
 
inline
const std::string ConfigHolder::mgrFile() const {
  return rootDirectory + "freeRecite.mgr";
}

inline
const std::string ConfigHolder::localDictFile() const {
  return rootDirectory + "freeRecite.dict";
}

inline
const std::string ConfigHolder::globalDictFile() const {
  return "/usr/share/FreeRecite/freeRecite.dict";
}

inline
const std::string ConfigHolder::keyFile() const {
  return rootDirectory + "keystone.txt";
}

}//End of namespace freeRecite.

#endif

