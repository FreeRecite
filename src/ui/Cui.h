/**
 * FileName: Cui.h.
 * Used to define the class CUI which is used to handle the UI.
 *
 * Copyright (C) 2008 Kermit Mei <kermit.mei@gmail.com>
 * All Rights Reserved.
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation.
 *
 * Written by Kermit Mei <kermit.mei@gmail.com>
 *
 * Many of the ideas implemented here are from the author's experiment.
 * But the dictionary's format coincide with the other word recite software
 * to help the users get more available data. And the review times designed
 * by means of the theory named Forgetting Curve which dicoveried by the
 * German psychologist named Hermann Ebbinghaus(1850–1909).
 *
 **/ 
#include <string>

#ifndef FREERECITE__CUI_H
#define FREERECITE__CUI_H

#include <vector>

namespace freeRecite {

class Scanner;

class CUI
{
public:
  CUI();
  ~CUI();
  void run(int argc, char *argv[]);

private:
  time_t atoid(const char *argv);
  void cleanStress();
  void clear();
  void createNew(const char *fileName);
  void exportFromFile(const char *fileName);
  void exportDone();
  void exportTask(time_t taskID);
  void exportStress();
  void help();
  void merge(const char *fileName = 0);
  bool modify(const std::string &word);
  void recite(const char *fileName, bool reverse);
  void recite(time_t taskID, bool reverse);
  void remove(time_t taskID);
  void r_scanProcess(Scanner &scanner);
  void scanProcess(Scanner &scanner);
  void scanProHelp();
  void showActive();
  void showAll();
  void showResult(bool result);
  void status();
  void test(const char *fileName, bool reverse);
  void test(time_t taskID, bool reverse);
};

} //Namespace freeRecite end.

#endif
